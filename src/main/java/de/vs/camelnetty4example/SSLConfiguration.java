package de.vs.camelnetty4example;

import org.apache.camel.component.netty4.NettyComponent;
import org.apache.camel.component.netty4.NettyConfiguration;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.util.jsse.KeyManagersParameters;
import org.apache.camel.util.jsse.KeyStoreParameters;
import org.apache.camel.util.jsse.SSLContextParameters;
import org.apache.camel.util.jsse.TrustManagersParameters;

public class SSLConfiguration {

	public static void addSSLto(DefaultCamelContext context) {

		KeyStoreParameters ksp = new KeyStoreParameters();
		ksp.setResource("ssl/keystore.jks");
		ksp.setPassword("zork42");

		KeyManagersParameters kmp = new KeyManagersParameters();
		kmp.setKeyStore(ksp);
		kmp.setKeyPassword("zork42");

		TrustManagersParameters tmp = new TrustManagersParameters();
		tmp.setKeyStore(ksp);

		SSLContextParameters scp = new SSLContextParameters();
		scp.setKeyManagers(kmp);
		scp.setTrustManagers(tmp);

		NettyComponent nettyComponent = context.getComponent("netty4", NettyComponent.class);
		NettyConfiguration nettyConfiguration = new NettyConfiguration();
		nettyConfiguration.setSslContextParameters(scp);
		nettyComponent.setConfiguration(nettyConfiguration);
	}
}

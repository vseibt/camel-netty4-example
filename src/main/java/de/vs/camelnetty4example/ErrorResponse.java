package de.vs.camelnetty4example;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

@SuppressWarnings("serial")
public class ErrorResponse implements Response<String> {

	private Status status = Status.ERROR;
	private String value;

	public ErrorResponse(String value) {
		this.value = value;
	}

	@Override
	public Status getStatus() {
		return status;
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("status", status).append("value", value)
				.toString();
	}
}

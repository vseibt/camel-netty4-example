package de.vs.camelnetty4example;

import java.io.Serializable;

public interface Response<T> extends Serializable {

	public enum Status {OK, ERROR};

	Status getStatus();

	T getValue();

}

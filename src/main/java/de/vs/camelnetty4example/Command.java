package de.vs.camelnetty4example;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

@SuppressWarnings("serial")
public class Command implements Serializable {

	private String name;
	private Map<String, Object> parameters = new HashMap<>();

	public Command(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Map<String, Object> getParameters() {
		return parameters;
	}

	public void addParameter(String name, Object value) {
		parameters.put(name, value);
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("name", name).append("parameters", parameters)
				.toString();
	}

}

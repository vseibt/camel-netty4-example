package de.vs.camelnetty4example;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

@SuppressWarnings("serial")
public class ValueResponse<T> implements Response<T> {

	private Status status = Status.OK;
	private T value;

	public ValueResponse(T value) {
		this.value = value;
	}

	@Override
	public Status getStatus() {
		return status;
	}

	@Override
	public T getValue() {
		return value;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("status", status).append("value", value)
				.toString();
	}
}

package de.vs.camelnetty4example;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class AESEncryption {

	private static final byte[] SALT = "12345678".getBytes();
	private static final IvParameterSpec IV_PARAMETER_SPEC = new IvParameterSpec("Abcdefgh12345678".getBytes());

	private SecretKey getKey(String password) throws GeneralSecurityException {
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
		KeySpec spec = new PBEKeySpec(password.toCharArray(), SALT, 65536, 256);
		SecretKey tmp = factory.generateSecret(spec);
		return new SecretKeySpec(tmp.getEncoded(), "AES");
	}

	public String encrypt(String password, String text) throws GeneralSecurityException, UnsupportedEncodingException {
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, getKey(password), IV_PARAMETER_SPEC);
		byte[] encrypted = cipher.doFinal(text.getBytes("UTF-8"));
		return new String(Base64.getEncoder().encode(encrypted));
	}

	public String decrypt(String password, String message) throws GeneralSecurityException, UnsupportedEncodingException {
		byte[] decoded = Base64.getDecoder().decode(message.getBytes());
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, getKey(password), IV_PARAMETER_SPEC);
		return new String(cipher.doFinal(decoded), "UTF-8");
	}
}

package de.vs.camelnetty4example;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class ServerMain {

	public static void main(String[] args) throws Exception {

		RouteBuilder routeBuilder = new RouteBuilder() {
			@Override
			public void configure() throws Exception {
				from("netty4:tcp://localhost:12345?ssl=true")
					.process(exchange -> processCommand(exchange, getCommand(exchange)));
			}
		};

		DefaultCamelContext context = new DefaultCamelContext();
		SSLConfiguration.addSSLto(context);
		context.addRoutes(routeBuilder);
		context.start();
	}

	private static String getCommand(Exchange exchange) {
		System.out.println("Exchange : " + exchange.getIn());
		Command msg = exchange.getIn().getBody(Command.class);
		System.out.println("Body     : " + msg);
		String cmd = msg.getName();
		System.out.println("CMD      : " + cmd);
		return cmd;
	}

	private static void processCommand(Exchange exchange, String cmd) {
		if("GET_STATISTICS".equals(cmd)) {
			Map<String, Object> map = new HashMap<>();
			map.put("dataSource.maxStatements",	0);
			map.put("dataSource.minPoolSize",5);
			map.put("dataSource.idleConnectionTestPeriod", 500);
			map.put("dataSource.maxIdleTime", 900);
			map.put("hibernate.sql.dialect", "de.vs.db.dialect.MyPostgreSqlDialect");
			map.put("dataSource.user", "zork");
			map.put("casServerLogoutUrl", "logout");
			map.put("dataSource.maxPoolSize", 18);
			map.put("dataSource.jdbcUrl", "jdbc:postgresql://postgres-zork-java8:5432/zork?ApplicationName=zork-web");
			map.put("hibernate.showSql", false);
			map.put("dataSource.driverClass", "org.postgresql.Driver");
			map.put("casServerLoginUrl", "login");
			map.put("dataSource.password", "****");
			exchange.getOut().setBody(new ValueResponse<Map<String, Object>>(map));
		} else {
			exchange.getOut().setBody(new ErrorResponse("Unbekanntes Kommando '" + cmd + "'."));
		}
	}
}

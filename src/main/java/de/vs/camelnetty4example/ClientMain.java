package de.vs.camelnetty4example;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class ClientMain {

	public static void main(String[] args) throws Exception {

		RouteBuilder routeBuilder = new RouteBuilder() {
			@Override
			public void configure() throws Exception {
				from("direct:in")
					.to("netty4:tcp://localhost:12345?disconnect=true&requestTimeout=10000&ssl=true");
			}
		};

		DefaultCamelContext context = new DefaultCamelContext();
		SSLConfiguration.addSSLto(context);
		context.addRoutes(routeBuilder);
		context.start();

		try {
			String cmd = "GET_STATISTICS";
			Response<?> response = context.createProducerTemplate().requestBody("direct:in", new Command(cmd), Response.class);
			System.out.println(response);

			sleep(10000L);

			cmd = "ZORK";
			response = context.createProducerTemplate().requestBody("direct:in", new Command(cmd), Response.class);
			System.out.println(response);

			sleep(10000L);
		} finally {
			context.stop();
		}
	}

	private static void sleep(Long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
